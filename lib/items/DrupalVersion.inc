<?php
/**
 * @file
 * DrupalVersionItem class.
 *
 * The current version of Drupal.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Drupal version item.
 */
class DrupalVersionItem extends ArchimedesItem
{
  /**
   * Gets the version string of Drupal
   *
   * @return string
   *   Version
   */
  public function get()
  {
    // Use the constant defined by Drupal
    return VERSION;
  }
}
