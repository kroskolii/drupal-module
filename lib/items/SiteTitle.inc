<?php
/**
 * @file
 * SiteTitleItem class.
 *
 * The title of this site - i.e. the Drupal site name.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Site title item.
 */
class SiteTitleItem extends ArchimedesItem {
  /**
   * Gets the site name.
   *
   * @return string
   *   Site name.
   */
  public function get() {
    return variable_get('site_name', FALSE);
  }

}
