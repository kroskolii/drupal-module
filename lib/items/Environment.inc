<?php
/**
 * @file
 * EnvironmentItem class.
 *
 * The current environment, as set by the Environment module.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Site environment class.
 */
class EnvironmentItem extends ArchimedesItem
{
  /**
   * Gets an array of the current environment name and label
   *
   * @return array
   *   Environment name and label
   */
  public function get()
  {
    $env = environment_current(NULL,NULL,TRUE);
    $env = array_shift($env);
    return array('Label'=>$env['label'], 'Name'=>$env['name']);
  }

  /**
   * Gets a string denoting the current environment and it's label
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    extract($this->get());
    return "$Label ($Name)";
  }
}
