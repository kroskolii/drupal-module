<?php
/**
 * @file
 * ReportGeneratedItem class.
 *
 * Generation time of the report.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Report generation time item.
 */
class ReportGeneratedItem extends ArchimedesItem {
  /**
   * Gets the generation time.
   *
   * @return int
   *   Generation time as a UNIX timestamp.
   */
  public function get() {
    return time();
  }

  /**
   * Gets the generation time formatted to RFC 2822.
   *
   * @return string
   *   HTML markup.
   */
  public function render() {
    return date('r', $this->get());
  }

}
