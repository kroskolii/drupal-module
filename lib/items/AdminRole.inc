<?php
/**
 * @file
 * AdminRoleItem class.
 *
 * Determines the Administrator role and grabs some basic details of users that
 * have it.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Administrator role item.
 */
class AdminRoleItem extends ArchimedesItem
{
  /**
   * Gets an array of the administator role and user data
   *
   * @return array
   *   Role and user data
   */
  public function get()
  {
    $rid = variable_get('user_admin_role', 0);
    if ($rid && $role = user_role_load($rid))
    {
      // Get the admin role details
      $name = $role->name;

      // Make a list of users that thave this role
      $query = 'SELECT DISTINCT(ur.uid) FROM {users_roles} AS ur WHERE ur.rid = :rid';
      $result = db_query($query, array(':rid' => $rid));
      $uids = $result->fetchCol();

      $users = array();
      $objs = user_load_multiple($uids);
      foreach ($objs as $user)
      {
        // Grab a subset of user data to send to the server
        $users[$user->uid] = array(
          'Name' => $user->name,
          'Mail' => $user->mail,
          'Status' => $user->status,
          'LastLogin' => $user->login,
        );
      }
    }
    else
    {
      $name = '';
      $users = array();
    }

    return array(
      'RoleName' => $name,
      'RoleID'   => $rid,
      'RoleUsers' => $users,
    );
  }

  /**
   * Gets a string denoting the number of nodes and revisions on the site
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    extract($this->get());
    if (!empty($RoleName))
    {
      $num = count($RoleUsers);
      $word = ($num == 1) ? 'user' : 'users';
      return "The admin role '{$RoleName}' (ID=$RoleID) contains $num $word";
    }
    else
    {
      return 'No admin role could be determined';
    }
  }
}
