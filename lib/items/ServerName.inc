<?php
/**
 * @file
 * ServerNameItem class.
 *
 * A URL that identifies this server.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Server name (url) item.
 */
class ServerNameItem extends ArchimedesItem {
  /**
   * Gets the home URL of this Drupal site.
   *
   * @return string
   *   URL
   */
  public function get() {
    // If Drush (or some other CLI passed method) is used to
    // run cron. You'll need to ensure a correct servername is
    // passed to PHP. With drush, use -l http://mysite.com.
    return url('', array('absolute' => 1));
  }

}
