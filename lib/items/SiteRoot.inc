<?php
/**
 * @file
 * SiteRootItem class.
 *
 * Drupal's root directory on the server's file system.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Site root directory item.
 */
class SiteRootItem extends ArchimedesItem
{
  /**
   * Gets the path to Drupal's root
   *
   * @return string
   *   Directory path
   */
  public function get()
  {
    // Use the constant defined by Drupal
    return DRUPAL_ROOT;
  }
}
