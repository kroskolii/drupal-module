<?php
/**
 * @file
 * ThemesItem class.
 *
 * Currently installed and enabled themes.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Installed themes item.
 */
class ThemesItem extends ArchimedesItem {
  /**
   * Gets an array of themes, keyed numerically.
   *
   * @return array
   *   Themes installed.
   */
  public function get() {

    $themes = array();
    $result = db_query("SELECT name FROM {system} WHERE status = 1 AND type = 'theme'");
    while ($theme = $result->fetchField()) {
      $info = drupal_parse_info_file(drupal_get_path('theme', $theme) . '/' . $theme . '.info');
      $themes[] = array(
        'Theme'       => $theme,
        'Name'        => (isset($info['name']) ? $info['name'] : ''),
        'Description' => (isset($info['description']) ? $info['description'] : ''),
        'Version'     => (isset($info['version']) ? $info['version'] : ''),
        'Project'     => (isset($info['project']) ? $info['project'] : ''),
        'Url'         => (isset($info['project status url']) ? $info['project status url'] : ''),
      );
    }
    return $themes;
  }

  /**
   * Gets a string denoting the number of themes installed.
   *
   * @return string
   *   HTML markup
   */
  public function render() {
    $count = count($this->get());
    $p = ($count == 1) ? 'theme' : 'themes';
    return "$count $p";
  }

}
