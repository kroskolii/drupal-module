<?php
/**
 * @file
 * SiteKeyItem class.
 *
 * The random key that distinguishes this site on the Archimedes server.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Site key item.
 */
class SiteKeyItem extends ArchimedesItem {
  /**
   * Gets the site key.
   *
   * @return string
   *   Site key.
   */
  public function get() {
    return variable_get('archimedes_client_site_key', FALSE);
  }

}
