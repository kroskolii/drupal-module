<?php
/**
 * @file
 * ServerHostnameItem class.
 *
 * Hostname of the server this instance of Drupal is running on.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Server hostname item.
 */
class ServerHostnameItem extends ArchimedesItem {
  /**
   * Gets the machien hostname using PHPs gethostname().
   *
   * @return string
   *   Hostname
   */
  public function get() {
    return gethostname();
  }

}
