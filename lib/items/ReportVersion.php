<?php
/**
 * @file
 * ReportVersionItem class.
 *
 * Version of system sending the report
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Report Version item.
 */
class ReportVersionItem extends ArchimedesItem {
  /**
   * Gets the machine name of this report Version.
   *
   * @return string
   *   Report Version.
   */
  public function get() {
    // Fixed value.
    return '1.0.0';
  }

  /**
   * Gets a human readable name for this report Version.
   *
   * @return string
   *   Report Version.
   */
  public function render() {
    // Fixed value.
    return '1.0.0';
  }

}
