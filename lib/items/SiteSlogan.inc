<?php
/**
 * @file
 * SiteSloganItem class.
 *
 * The slogan or tagline of this site.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Site slogan item.
 */
class SiteSloganItem extends ArchimedesItem {
  /**
   * Gets the site slogan or tagline.
   *
   * @return string
   *   Slogan.
   */
  public function get() {
    return variable_get('site_slogan', FALSE);
  }

}
