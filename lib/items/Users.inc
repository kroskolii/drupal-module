<?php
/**
 * @file
 * UsersItem class.
 *
 * The number of site users currently configured.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Site users item.
 */
class UsersItem extends ArchimedesItem {
  /**
   * Gets the count of site users.
   *
   * @return int
   *   Number of users.
   */
  public function get() {
    return db_query("SELECT COUNT(uid) as count FROM {users}")->fetchField();
  }

}
