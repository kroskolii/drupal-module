<?php
/**
 * @file
 * ModulesItem class.
 *
 * Currently installed and enabled modules.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Installed modules item.
 */
class ModulesItem extends ArchimedesItem {
  /**
   * Gets an array of modules, keyed numerically
   *
   * @return array
   *   Modules
   */
  public function get()
  {
    $modules = array();
    foreach (module_list() as $module)
    {
      $info = drupal_parse_info_file(drupal_get_path('module', $module) . '/' . $module . '.info');
      $modules[] = array(
        'Module'      => $module,
        'Name'        => (isset($info['name']) ? $info['name'] : ''),
        'Description' => (isset($info['description']) ? $info['description'] : ''),
        'Package'     => (isset($info['package']) ? $info['package'] : ''),
        'Project'     => (isset($info['project']) ? $info['project'] : ''),
        'Version'     => (isset($info['version']) ? $info['version'] : ''),
        'Url'         => (isset($info['project status url']) ? $info['project status url'] : ''),
      );
    }
    return $modules;
  }

  /**
   * Gets a string denoting the number of modules installed
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    return "".count($this->get())." modules";
  }
}
