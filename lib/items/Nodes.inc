<?php
/**
 * @file
 * NodesItem class.
 *
 * Counts of all nodes and revisions (no their content).
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Content nodes item.
 */
class NodesItem extends ArchimedesItem {
  /**
   * Gets an array of the node and revisions counts.
   *
   * @return array
   *   Counts
   */
  public function get() {
    $nodes = db_query("SELECT COUNT(uid) as count FROM {node}")->fetchField();
    $revisions = db_query("SELECT COUNT(uid) as count FROM {node_revision}")->fetchField();
    return array(
      'Nodes' => $nodes,
      'Revisions' => $revisions,
    );
  }

  /**
   * Gets a string denoting the number of nodes and revisions on the side.
   *
   * @return string
   *   HTML markup
   */
  public function render() {

    $data = $this->get();

    $p1 = ($data['Nodes'] == 1) ? '' : 's';
    $p2 = ($data['Revisions'] == 1) ? '' : 's';

    $data['Nodes'] = ($data['Nodes'] == 0) ? '0' : $data['Nodes'];
    $data['Revisions'] = ($data['Revisions'] == 0) ? '0' : $data['Revisions'];

    return t('@nodes node@nodes_plural, @revisions revision@revisions_plural',
      array(
        '@nodes' => $data['Nodes'],
        '@revisions' => $data['Revisions'],
        '@nodes_plural' => $p1,
        '@revisions_plural' => $p2,
      ));
  }

}
