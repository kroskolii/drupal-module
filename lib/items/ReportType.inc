<?php
/**
 * @file
 * ReportTypeItem class.
 *
 * Type of system sending the report - in this case Drupal.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Report type item.
 */
class ReportTypeItem extends ArchimedesItem {
  /**
   * Gets the machine name of this report type.
   *
   * @return string
   *   Report type.
   */
  public function get() {
    // Fixed value.
    return 'drupal7';
  }

  /**
   * Gets a human readable name for this report type.
   *
   * @return string
   *   Report type.
   */
  public function render() {
    // Fixed value.
    return 'Drupal 7';
  }

}
