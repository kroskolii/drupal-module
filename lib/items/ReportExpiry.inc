<?php
/**
 * @file
 * ReportExpiryItem class.
 *
 * Expiry time of the report, to prevent replay-attacks.
 *
 * @package Archimedes
 *
 * @subpackage Client
 */

/**
 * Report expiry time item.
 */
class ReportExpiryItem extends ArchimedesItem {
  /**
   * Gets the expiry time.
   *
   * @return int
   *   Expiry time as a UNIX timestamp.
   */
  public function get() {
    return time() + variable_get('archimedes_client_report_expiry', 1800);
  }

  /**
   * Gets the expiry time formatted to RFC 2822.
   *
   * @return string
   *   HTML markup.
   */
  public function render() {
    return date('r', $this->get());
  }

}
