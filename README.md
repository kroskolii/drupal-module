# Mataara Drupal Client

A module for Drupal that communicates site, module, and security information to a Mataara server instance.

Please note that the source code and other documentation in this repository still refers to the project by its previous name of Archimedes.


You probably want to use the module version from the branch 6.x, 7.x or 8.x depending on the version of Drupal you are using.
