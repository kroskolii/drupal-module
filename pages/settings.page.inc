<?php
/**
 * @file
 * Callbacks for the settings page.
 */

/**
 * Administration form callback for the settings page.
 *
 * @return array
 *   A drupal system settings form.
 */
function archimedes_client_page_settings() {

  $form = array();
  $form['report_method'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reporting Method'),
    '#description' => t('Archimedes can be configured to send reports to the server over different channels. Reports can be emailed as attachments to a mailbox monitored by the server, or posted directly to the server over HTTP. All reporting methods are encrypted.'),
    '#collabsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['report_method']['archimedes_client_server_method'] = array(
    '#type' => 'radios',
    '#title' => t('Reporting Method'),
    '#default_value' => variable_get('archimedes_client_server_method', 'email'),
    '#options' => array(
      'email' => t('Email'),
      'http' => t('HTTP'),
    ),
  );
  $form['report_method']['archimedes_client_server_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Server email address'),
    '#default_value' => variable_get('archimedes_client_server_email', FALSE),
    '#description' => t('Email address of a mailbox monitored by the Archimedes server.'),

    // Only visible when 'method' set to Email.
    '#states' => array(
      'visible' => array(
        ':input[name=archimedes_client_server_method]' => array('value' => 'email'),
      ),
    ),
  );
  $form['report_method']['archimedes_client_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#default_value' => variable_get('archimedes_client_server_url', FALSE),
    '#description' => t("URL of the Archimedes server's HTTP post endpoint"),

    // Only visible when 'method' set to HTTP.
    '#states' => array(
      'visible' => array(
        ':input[name=archimedes_client_server_method]' => array('value' => 'http'),
      ),
    ),
  );
  $form['report_frequency'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reporting Frequency'),
    '#description' => t("Reports are sent to the server periodically, trigged by Drupal's cron runs. On each cron run, Archimedes will check to see if the interval since the last report exceeds this configured interval, and if so it will send the next report."),
    '#collabsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['report_frequency']['archimedes_client_cron_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Reporting Interval (seconds)'),
    '#description' => t("Number of seconds between Archimedes reports. This is lower bound by the frequency Drupal's cron is run"),
    '#size' => 5,
    '#default_value' => variable_get('archimedes_client_cron_interval', 86400),
  );
  $form['security'] = array(
    '#type' => 'fieldset',
    '#title' => t('Security'),
    '#description' => t('Archimedes uses a public/private key pair to encrypt data during transmission (to prevent eavesdropping). Encrypted reports include an expiry time in order to minimise the effect of a replay attack.'),
    '#collabsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['security']['archimedes_client_pubkey'] = array(
    '#type' => 'textarea',
    '#title' => t('Public Key'),
    '#default_value' => variable_get('archimedes_client_pubkey', FALSE),
    '#description' => t('The public key used to encrypt the reports sent to the server.'),
  );
  $form['security']['archimedes_client_report_expiry'] = array(
    '#type' => 'textfield',
    '#title' => t('Report Expiry Time (seconds)'),
    '#default_value' => variable_get('archimedes_client_report_expiry', 1800),
    '#size' => 5,
    '#description' => t('Number of seconds after sending that a report becomes invalid. The server must have enough time to receive the report before it expires, however the expiry should be set to a minimal value to minimise the effect of a reply attack.'),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_validate().
 *
 * Validate hook for the settings page form.
 */
function archimedes_client_page_settings_validate($node) {

  // Ensure the pubkey is valid.
  $key = $node['security']['archimedes_client_pubkey']['#value'];
  if (!empty($key) && !openssl_pkey_get_public($key)) {
    form_set_error('archimedes_client_pubkey', t('The public key you provided is invalid'));
  }
}
