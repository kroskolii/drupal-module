<?php
/**
 * @file
 * Callbacks for the report page.
 */

/**
 * Callback for the report page.
 */
function archimedes_client_page_report() {

  // Create a report instance.
  $report = new ArchimedesReport();

  // Set JSON mime type.
  header('Content-Type: application/json');

  // Determine if pretty-print required or not, and output.
  $pretty = isset($_GET['pretty']);
  echo $report->getJson($pretty);

  exit;
}
