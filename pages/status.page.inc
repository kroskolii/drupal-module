<?php
/**
 * @file
 * Callbacks for the status page.
 */

/**
 * Callback for the status page.
 *
 * @return string
 *   HTML content
 */
function archimedes_client_page_status() {

  // Get the reporting method.
  $m = variable_get('archimedes_client_server_method', 'email');
  switch ($m) {
    case 'http':
      $url = variable_get('archimedes_client_server_url', t('no URL (configure one now!)'));
      $method = t('HTTP - posted to @url', array('@url' => $url));
      break;

    case 'email':
    default:
      $email = variable_get('archimedes_client_server_email', t('no address (configure one now!)'));
      $method = t('Email - sent to @address', array('@address' => $email));
      break;
  }

  // Get the report frequency.
  $interval = variable_get('archimedes_client_cron_interval', 86400);
  if ($interval > 86400) {
    $n = $interval / 86400;
    $freq = ($n == 1) ? t('day') : t('@n days', array('@n' => $n));
  }
  elseif ($interval > 3600) {
    $n = $interval / 3600;
    $freq = ($n == 1) ? t('hour') : t('@n hours', array('@n' => $n));
  }
  else {
    $n = $interval;
    $freq = ($n == 1) ? t('second') : t('@n seconds', array('@n' => $n));
  }

  // Get the last report time.
  $last_run = variable_get('archimedes_client_last_report_sent', 0);
  $last_d = date('r', $last_run);
  $last = ($last_run == 0) ? t('No reports have been sent yet!') : t('The last report was sent at @last_date', array('@last_date' => $last_d));

  // Get the next report time.
  $next_run = $last_run + variable_get('archimedes_client_cron_interval', 86400);
  $next_d = date('r', $next_run);
  $next = ($last_run == 0) ? t('as soon as possible') : t('after @next_date', array('@next_date' => $next_d));

  // Fetch rendered report items.
  $report = new ArchimedesReport();
  $data = $report->getRendered();

  // Generate table items.
  $rows = array();
  foreach ($data as $k => $v) {
    // Add URL awareness.
    if (substr($v, 0, 7) === "http://") {
      $rendered_v = "<a href=\"$v\">$v</a>";
    }
    else {
      $rendered_v = $v;
    }

    $rows[] = array($k, array(
      'data' => $rendered_v,
      'id' => $k,
      'class' => 'report-field',
      ),
    );
  }
  $table = array(
    'header' => array(t('Item'), t('Value')),
    'rows' => $rows,
    'attributes' => array('id' => 'report'),
  );

  // Build the page HTML.
  $html  = '';

  $html .= '<b>Report Method</b>';
  $html .= "<p>$method</p>";

  $html .= '<b>Report Frequency</b>';
  $html .= "<p>Reports sent every $freq</p>";

  $html .= '<b>Last Report</b>';
  $html .= "<p>$last</p>";

  $html .= '<b>Next Report</b>';
  $html .= "<p>Next report will be run $next</p>";

  $html .= '<b>Report Data</b>';
  $html .= theme('table', $table);
  $html .= "<p>View as ";
  $html .= l(t('Raw JSON'), 'admin/config/archimedes_client/report.json');
  $html .= " or ";
  $html .= l(t('Pretty JSON'), 'admin/config/archimedes_client/report.json', array('query' => array('pretty' => NULL)));
  $html .= "</p>";

  $html .= "<p>";
  $html .= l(t('Change Archimedes Client settings...'), 'admin/config/archimedes_client/settings');
  $html .= "</p>";

  return $html;
}
