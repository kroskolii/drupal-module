<?php
/**
 * @file
 * Drush integration for the archimedes module.
 */

/**
 * Implements hook_drush_command().
 */
function archimedes_client_drush_command() {

  $items['arch-report'] = array(
    'description' => dt('Display an Archimedes Client report.'),
    'options' => array(
      'format' => dt('Rendered (human readable), JSON, or encrypted JSON'),
    ),
    'examples' => array(
      'drush arch-report' => dt('Display a report.'),
      'drush arch-report json' => dt('Display a report in JSON format.'),
      'drush arch-report encrypted > report.enc' => dt('Save an encypted report to file.'),
    ),
    'aliases' => array('ar'),
  );
  $items['arch-send'] = array(
    'description' => dt('Send an Archimedes Client report.'),
    'options' => array(
      'method' => dt('Email or HTTP'),
      'location' => dt('Email address or URL to send the report to (depending on chosen method)'),
    ),
    'examples' => array(
      'drush arch-send' => dt('Send a report using the configured method.'),
      'drush arch-send http' => dt('Send a report using HTTP.'),
    ),
    'aliases' => array('as'),
  );
  return $items;
}

/**
 * Check that the hostname has been provided to Drush using the -l flag.
 *
 * @return bool
 *    TRUE if the hostname was provided.
 */
function _archimedes_client_require_drush_hostname() {

  if (url('', array('absolute' => 1)) == 'http://default/') {
    drush_set_error('archimedes_client_hostname_not_set', dt("Hostname was not specified. Try adding '-l http://example.com'"));
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Drush command callback for arch-report.
 *
 * Generates and displays a report in the chosen (or default) format.
 *
 * @param string $format
 *   Choice of report format (Rendered, JSON, or Encrypted).
 * @param string $location
 *   The report endpoint location (e.g. Email or URL).
 */
function drush_archimedes_client_arch_report($format = 'rendered', $location = FALSE) {

  if (!_archimedes_client_require_drush_hostname()) {
    return;
  }

  drush_print($location);

  $report = new ArchimedesReport();
  switch (strtolower($format)) {
    case 'rendered':
    case 'render':
    case 'human':
    case 'human-readable':
    default:
      $data = $report->getRendered();
      $rows = array(array(dt('Item'), dt('Value')));
      foreach ($data as $k => $v) {
        $rows[] = array($k, $v);
      }
      drush_print_table($rows, TRUE);
      break;

    case 'json':
      drush_print($report->getJson());
      break;

    case 'json-pretty':
    case 'pretty-json':
      drush_print($report->getJson(TRUE));
      break;

    case 'encrypted':
    case 'encrypted-json':
    case 'ejson':
      drush_print($report->getEncrypted());
      break;
  }
}

/**
 * Drush command callback for arch-send.
 *
 * Generates and sends a report using the configured or given format and
 * endpoint location.
 *
 * @param string $method
 *   Reporting method to use (e.g. Email or HTTP).
 * @param string $location
 *   Endpoint location to use (e.g. an email address or URL).
 */
function drush_archimedes_client_arch_send($method = '', $location = '') {

  if (!_archimedes_client_require_drush_hostname()) {
    return;
  }

  if ($method && !in_array(strtolower($method), array('email', 'http'))) {
    drush_set_error('archimedes_client_send_invalid_method', 'Invalid report send method!');
    return;
  }

  $method_config = variable_get('archimedes_client_server_method', 'email');
  $email = variable_get('archimedes_client_server_email', FALSE);
  $url = variable_get('archimedes_client_server_url', FALSE);

  if (!$method) {
    $method = $method_config;
  }
  if (!$location && $method == 'email') {
    $location = $email;
  }
  if (!$location && $method == 'http') {
    $location = $url;
  }

  drush_print(dt('Sending @method report to @location', array('@method' => $method, '@location' => $location)));
  $report = new ArchimedesReport();
  $r = $report->send($method, $location);

  if ($r === TRUE) {
    drush_print(dt('Success!'));
  }
  else {
    // drush_set_error('archimedes_client_send_fail', $r);
    watchdog('archimedes_client', 'Could not send report via @method method. Reason: %error_msg',
      array(
        '@method'=> $method,
        '%error_msg' => $r
    ), WATCHDOG_ERROR);
  }
}
